MAKE            ?= make
RACKET          := racket
RACO            := $(RACKET) -l raco --
SCRIBBLE        := $(RACO) scribble

PWD             ?= $(shell pwd)
DOCS            := $(PWD)/docs
PUBLIC          := $(PWD)/public


.PHONY: all
all: compile

src-%:
	$(MAKE) -C src $(*)

.PHONY: clean
clean: src-clean

.PHONY: compile
compile: src-compile

.PHONY: install
install: src-install

.PHONY: setup
setup: src-setup

.PHONY: test
test: src-test

.PHONY: remove
remove: src-remove

$(PUBLIC):
	$(SCRIBBLE) ++main-xref-in --dest $(PWD) --dest-name public \
		--htmls --quiet $(DOCS)/scribblings/main.scrbl

.PHONY: public
public:
	$(MAKE) -B $(PUBLIC)
