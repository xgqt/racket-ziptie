# ZipTie

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/racket-ziptie">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/racket-ziptie/">
    </a>
    <a href="https://gitlab.com/xgqt/racket-ziptie/pipelines">
        <img src="https://gitlab.com/xgqt/racket-ziptie/badges/master/pipeline.svg">
    </a>
</p>

Racket library for unusual scenarios.


## About

Dirty tricks to get your Racket code running in unusual scenarios.

ZipTie allows you to more easily and securely bind/fuse/glue together Racket
and external build or runtime dependencies.


## License

Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
