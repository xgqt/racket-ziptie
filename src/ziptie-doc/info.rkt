#lang info


(define pkg-desc "ZipTie, documentation.")

(define version "0.0")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"))

(define build-deps
  '("racket-doc"
    "scribble-lib"
    "ziptie-git"))
