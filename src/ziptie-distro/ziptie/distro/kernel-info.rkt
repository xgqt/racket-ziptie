;; This file is part of racket-ziptie - Racket library for unusual scenarios.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ziptie is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ziptie is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ziptie.  If not, see <https://www.gnu.org/licenses/>.


;; This module uses "uname" underneath to detect the kernel name


#lang racket/base

(require "private/uname.rkt")

(provide uname-executable-path kernel-info)


(define (kernel-info)
  (hash 'name    (uname "-s" #rx"[A-z]+")
        'release (uname "-r" #rx"[A-z0-9\\.\\-]+")
        'version (uname "-v" #rx".+")))
