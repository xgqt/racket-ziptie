;; This file is part of racket-ziptie - Racket library for unusual scenarios.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ziptie is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ziptie is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ziptie.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/contract
         threading)

(provide (contract-out [nproc (-> exact-nonnegative-integer?)]))


(define (nproc)
  (case (system-type 'os*)
    [(linux)
     (with-handlers ([exn:fail? (lambda _ 1)])
       (~>> "/sys/devices/system/cpu/online"
            open-input-file
            read-line
            (regexp-split "-")
            (list-ref _ 1)
            string->number
            add1))]
    [else
     (error 'nproc "Unsupported system")]))
