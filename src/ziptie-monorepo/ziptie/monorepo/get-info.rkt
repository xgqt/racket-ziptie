;; This file is part of racket-ziptie - Racket library for unusual scenarios.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ziptie is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ziptie is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ziptie.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require racket/contract
         (prefix-in setup: (only-in setup/getinfo get-info/full)))

(provide (contract-out [get-info/hash (-> path-string? hash?)]))


(define-syntax-rule (my-make-hash proc prop ...)
  (make-hash
   (append (with-handlers ([exn:fail?  (lambda _ '())])
             (list (cons 'prop (proc 'prop)))) ...)))


(define (get-info/hash pth)
  (my-make-hash
   (setup:get-info/full pth)
   assume-virtual-sources
   build-deps
   clean
   collection
   compile-include-files
   compile-omit-files
   compile-omit-paths
   copy-foreign-libs
   copy-man-pages
   copy-shared-files
   deps
   doc-module-suffixes
   doc-module-suffixes
   gracket-launcher-flags
   gracket-launcher-libraries
   gracket-launcher-names
   install-collection
   install-platform
   license
   module-suffixes
   module-suffixes
   move-foreign-libs
   move-man-pages
   move-shared-files
   mred-launcher-flags
   mred-launcher-libraries
   mred-launcher-names
   mzscheme-launcher-flags
   mzscheme-launcher-libraries
   mzscheme-launcher-names
   name
   pkg-authors
   pkg-desc
   post-install-collection
   pre-install-collection
   racket-launcher-flags
   racket-launcher-libraries
   racket-launcher-names
   release-note-files
   requires
   scribblings
   test-command-line-arguments
   test-ignore-stderrs
   test-include-paths
   test-lock-names
   test-omit-paths
   test-randoms
   test-responsibles
   test-timeouts
   version))
