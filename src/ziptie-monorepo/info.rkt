#lang info


(define pkg-desc "ZipTie, monorepo component.")

(define version "0.0")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "upi-lib"
    "threading-lib"))
